package taxi.raheemdriver.com.raheemdriver.constants;

/**
 * Created by Aleesha Kanwal on 29/07/2018.
 */

public class Constants {

    public static final String FIREBASE_TOKEN = "firebasetoken";
    public static final String IMAGE_URL = "http://raheemcar.com/images/captains/";
}
