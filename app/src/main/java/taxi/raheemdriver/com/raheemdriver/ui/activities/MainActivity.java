package taxi.raheemdriver.com.raheemdriver.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import taxi.raheemdriver.com.raheemdriver.R;
import taxi.raheemdriver.com.raheemdriver.sharedprefrences.SharedPrefrences;
import taxi.raheemdriver.com.raheemdriver.ui.fragments.FragmentMap;
import taxi.raheemdriver.com.raheemdriver.util.MessageDialog;

public class MainActivity extends AppCompatActivity {

    DrawerLayout myDrawerLayout;
    NavigationView myNavigationView;
    FragmentManager myFragmentManager;
    FragmentTransaction myFragmentTransaction;
    private Toolbar toolbar;
    FragmentMap fragmentMap;
    boolean isOffline;
    Menu menuStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.my_action_bar);
        setSupportActionBar(toolbar);
        fragmentMap = new FragmentMap();

       /* if (getIntent().getStringExtra("offline") != null) {
            String offline = getIntent().getStringExtra("offline");

            if (offline.equals("yes")) {

                showAlertPopup(this, "Go Offline now? This mean you will miss out on new booking");
            }
        }*/

      //  checkStatusCaptain();

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        myDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        myNavigationView = (NavigationView) findViewById(R.id.nav_drawer);
        myNavigationView.setItemIconTintList(null);

        myFragmentManager = getSupportFragmentManager();
        myFragmentTransaction = myFragmentManager.beginTransaction();
        myFragmentTransaction.replace(R.id.containerView, fragmentMap).commit();

        myNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem selectedMenuItem) {
                myDrawerLayout.closeDrawers();


                if (selectedMenuItem.getItemId() == R.id.logout) {
                    fragmentMap.captainStatus(2);
                    SharedPrefrences.putBoolean("login", false, getApplicationContext());
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                }

                return false;
            }
        });

        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, myDrawerLayout, toolbar, R.string.app_name,
                R.string.app_name);

        myDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
    }


    public void showAlertPopup(final Activity context, String message) {

        final MessageDialog dialog = new MessageDialog(context, message,
                -1, R.string.dialog_edit_no_text, R.string.dialog_edit_yes_text);
        dialog.setCancelable(false);
        if (!context.isFinishing()) {
            dialog.show();
            dialog.yesButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    fragmentMap.goOffline();
                    isOffline = true;
                    menuStatus.getItem(0).setTitle("Go Online");

                    dialog.cancel();
                }
            });

            dialog.noButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.cancel();
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_status, menu);
        menuStatus = menu;

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if(item.getTitle().equals("Go Online")){

            fragmentMap.goOnline();
            menuStatus.getItem(0).setTitle("Go Offline");
        }
       else if (id == R.id.action_settings) {

            SharedPrefrences.putString("offline","yes",getApplicationContext());
            checkStatusCaptain();
        }

        return super.onOptionsItemSelected(item);
    }

    private void checkStatusCaptain(){
        if (SharedPrefrences.getString("offline",getApplicationContext()).equals("yes")) {
            String offline = SharedPrefrences.getString("offline",getApplicationContext());

            if (offline.equals("yes")) {

                showAlertPopup(this, "Go Offline now? This mean you will miss out on new booking");
            }
        }
    }
}