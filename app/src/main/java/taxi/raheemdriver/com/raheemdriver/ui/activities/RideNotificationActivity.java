package taxi.raheemdriver.com.raheemdriver.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.TextView;

import com.github.johnpersano.supertoasts.library.Style;
import com.github.johnpersano.supertoasts.library.SuperActivityToast;

import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import taxi.raheemdriver.com.raheemdriver.R;
import taxi.raheemdriver.com.raheemdriver.models.Captainresponse;
import taxi.raheemdriver.com.raheemdriver.networking.JsonApiInterface;
import taxi.raheemdriver.com.raheemdriver.networking.RetrofitClient;
import taxi.raheemdriver.com.raheemdriver.sharedprefrences.SharedPrefrences;
import taxi.raheemdriver.com.raheemdriver.util.InternetConnection;
import taxi.raheemdriver.com.raheemdriver.util.Utility;

/**
 * Created by Aleesha Kanwal on 13/10/2018.
 */
public class RideNotificationActivity extends AppCompatActivity {

    TextView from, to, acceptRequestTime;
    AppCompatButton acceptBtn, rejectBtn;
    JsonApiInterface jsonApiInterface;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_ride);
        from = (TextView) findViewById(R.id.from);
        to = (TextView) findViewById(R.id.to);
        acceptRequestTime = (TextView) findViewById(R.id.acceptRequestTime);
        acceptBtn = (AppCompatButton) findViewById(R.id.acceptBtn);
        rejectBtn = (AppCompatButton) findViewById(R.id.rejectBtn);

        jsonApiInterface = RetrofitClient.getClient().create(JsonApiInterface.class);

    //    Bundle msg = SharedPrefrences.getInstance().getBundle();

        String fromLoc =  SharedPrefrences.getString("from_loc",getApplicationContext());
        String toLoc =  SharedPrefrences.getString("to_loc",getApplicationContext());
        from.setText(fromLoc);
        to.setText(toLoc);


        final CountDownTimer yourCountDownTimer =  new CountDownTimer(16000, 1000) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished) {
                acceptRequestTime.setText("Request accept in " + String.format("%d : %d ",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {
                acceptRequestTime.setText("Ride Rejected");
                SharedPrefrences.putBoolean("startRide", false, getApplicationContext());
                SharedPrefrences.putBoolean("rideAccept", false, getApplicationContext());
                SharedPrefrences.putBoolean("endRide", false, getApplicationContext());
                SharedPrefrences.putBoolean("driverConfirmed",false,getApplicationContext());
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        }.start();


        acceptBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (yourCountDownTimer != null) {
                    yourCountDownTimer.cancel();
                }

                SharedPrefrences.putBoolean("startRide", false, getApplicationContext());
                SharedPrefrences.putBoolean("endRide", false, getApplicationContext());
                SharedPrefrences.putBoolean("driverConfirmed",false,getApplicationContext());
                SharedPrefrences.putBoolean("rideAccept",true,getApplicationContext());

               captainResponse(1);
            }
        });

        rejectBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (yourCountDownTimer != null) {
                    yourCountDownTimer.cancel();
                }

                SharedPrefrences.putBoolean("startRide", false, getApplicationContext());
                SharedPrefrences.putBoolean("rideAccept", false, getApplicationContext());
                SharedPrefrences.putBoolean("endRide", false, getApplicationContext());
                SharedPrefrences.putBoolean("driverConfirmed",false,getApplicationContext());
                captainResponse(2);
            }
        });


    }


    public void captainResponse(int status) {

        Utility.onServiceSuccessNetworkCheck(this, InternetConnection.isNetworkAvailable(this));

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        String rideID = SharedPrefrences.getString("riderID", this);

        Call<Captainresponse> response = jsonApiInterface.captainResponse(rideID, status);
        response.enqueue(new Callback<Captainresponse>() {
            @Override
            public void onResponse(Call<Captainresponse> call, Response<Captainresponse> response) {
                Captainresponse userList = response.body();
                progressDialog.cancel();


                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);

                SuperActivityToast.create(RideNotificationActivity.this, new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText(userList.getMessage())
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setAnimations(Style.ANIMATIONS_POP).show();


            }

            @Override
            public void onFailure(Call<Captainresponse> call, Throwable t) {
                progressDialog.cancel();
                SuperActivityToast.create(RideNotificationActivity.this, new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText(t.getMessage())
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setAnimations(Style.ANIMATIONS_POP).show();
                call.cancel();
            }
        });
    }
}
