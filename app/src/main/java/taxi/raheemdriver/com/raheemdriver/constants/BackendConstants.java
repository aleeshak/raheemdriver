package taxi.raheemdriver.com.raheemdriver.constants;

/**
 * Created by Aleesha Kanwal on 09/11/2017.
 */

public class BackendConstants {

    public static final String LOGIN_CAPTAIN = "/api/captain_new_login";
    public static final String FORGOT_PASSWORD = "/api/reset_captain_password";
    public static final String STATUS_CAPTAIN = "/api/update_new_captain_status";
    public static final String NEW_CAPTAIN_RESPONSE = "/api/new_captain_response";
    public static final String UPDATE_DRIVER_POSITION = "/api/new_driver_position";
    public static final String CAPTAIN_ARRIVED = "/api/new_captain_arrived";
    public static final String END_RIDE = "/api/new_end_ride";
    public static final String COLLECT_FEE = "/api/new_client_credit";
    public static final String RATE_USER = "/api/save_new_client_rating";
    public static final String CANCEL_RIDE = "/api/cancel_ride";
}
