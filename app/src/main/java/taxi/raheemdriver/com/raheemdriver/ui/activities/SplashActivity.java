package taxi.raheemdriver.com.raheemdriver.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import com.github.johnpersano.supertoasts.library.Style;
import com.github.johnpersano.supertoasts.library.SuperActivityToast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import taxi.raheemdriver.com.raheemdriver.R;
import taxi.raheemdriver.com.raheemdriver.models.Status;
import taxi.raheemdriver.com.raheemdriver.networking.JsonApiInterface;
import taxi.raheemdriver.com.raheemdriver.networking.RetrofitClient;
import taxi.raheemdriver.com.raheemdriver.sharedprefrences.SharedPrefrences;
import taxi.raheemdriver.com.raheemdriver.util.GPSTracker;
import taxi.raheemdriver.com.raheemdriver.util.InternetConnection;
import taxi.raheemdriver.com.raheemdriver.util.Utility;


public class SplashActivity extends Activity {
	private static int SPLASH_TIME_OUT = 2000;
	JsonApiInterface jsonApiInterface;
	GPSTracker gpsTracker;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_splash);


		jsonApiInterface = RetrofitClient.getClient().create(JsonApiInterface.class);
		gpsTracker = new GPSTracker(getApplicationContext());

		new Handler().postDelayed(new Runnable() {

			/*
			 * Showing splash screen with a timer. This will be useful when you
			 * want to show case your app logo / company
			 */

			@Override
			public void run() {

				if(SharedPrefrences.getBoolean("login",getApplicationContext())) {
					captainStatus(1);
					Intent i = new Intent(SplashActivity.this, MainActivity.class);
					startActivity(i);
				}
				else {
					Intent i = new Intent(SplashActivity.this, LoginActivity.class);
					startActivity(i);
				}
				finish();
			}
		}, SPLASH_TIME_OUT);
	}


	public void captainStatus(int status) {

		Utility.onServiceSuccessNetworkCheck(SplashActivity.this, InternetConnection.isNetworkAvailable(getApplicationContext()));

		String driverId = SharedPrefrences.getInstance().getUserObject().getDriverId();

		Call<Status> response = jsonApiInterface.statusCaptain(status, driverId, gpsTracker.getLatitude(), gpsTracker.getLongitude());
		response.enqueue(new Callback<Status>() {
			@Override
			public void onResponse(Call<Status> call, Response<Status> response) {
				Status userList = response.body();

				SuperActivityToast.create(SplashActivity.this, new Style(), Style.TYPE_BUTTON)
						.setProgressBarColor(Color.WHITE)
						.setText(userList.getMessage())
						.setDuration(Style.DURATION_LONG)
						.setFrame(Style.FRAME_LOLLIPOP)
						.setColor(getResources().getColor(R.color.colorPrimary))
						.setAnimations(Style.ANIMATIONS_POP).show();


			}

			@Override
			public void onFailure(Call<Status> call, Throwable t) {

				SuperActivityToast.create(SplashActivity.this, new Style(), Style.TYPE_BUTTON)
						.setProgressBarColor(Color.WHITE)
						.setText(t.getMessage())
						.setDuration(Style.DURATION_LONG)
						.setFrame(Style.FRAME_LOLLIPOP)
						.setColor(getResources().getColor(R.color.colorPrimary))
						.setAnimations(Style.ANIMATIONS_POP).show();
				call.cancel();
			}
		});
	}


	@Override
	public void onBackPressed() {

	}
}
