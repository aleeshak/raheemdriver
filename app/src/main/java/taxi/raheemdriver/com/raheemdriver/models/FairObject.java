
package taxi.raheemdriver.com.raheemdriver.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FairObject {

    @SerializedName("distance")
    @Expose
    private Integer distance;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("ride_amt")
    @Expose
    private Integer rideAmt;
    @SerializedName("bal_after")
    @Expose
    private Integer balAfter;
    @SerializedName("pay_amount")
    @Expose
    private Integer payAmount;
    @SerializedName("bal_before")
    @Expose
    private Integer balBefore;

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Integer getRideAmt() {
        return rideAmt;
    }

    public void setRideAmt(Integer rideAmt) {
        this.rideAmt = rideAmt;
    }

    public Integer getBalAfter() {
        return balAfter;
    }

    public void setBalAfter(Integer balAfter) {
        this.balAfter = balAfter;
    }

    public Integer getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(Integer payAmount) {
        this.payAmount = payAmount;
    }

    public Integer getBalBefore() {
        return balBefore;
    }

    public void setBalBefore(Integer balBefore) {
        this.balBefore = balBefore;
    }

}
