package taxi.raheemdriver.com.raheemdriver.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import taxi.raheemdriver.com.raheemdriver.models.Updatelocation;
import taxi.raheemdriver.com.raheemdriver.networking.JsonApiInterface;
import taxi.raheemdriver.com.raheemdriver.networking.RetrofitClient;
import taxi.raheemdriver.com.raheemdriver.sharedprefrences.SharedPrefrences;
import taxi.raheemdriver.com.raheemdriver.util.GPSTracker;

/**
 * Created by Aleesha Kanwal on 16/11/2018.
 */
public class ServiceWithStatusThree extends Service {

    JsonApiInterface jsonApiInterface;
    GPSTracker gpsTracker;
    Context context;
    private Handler mHandler;
    private Runnable mRunnable;

    public void onCreate()
    {
        super.onCreate();
        context = this;


    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Let it continue running until it is stopped.
        Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();


        final long timer = 5000;

        mHandler = new Handler();
        mRunnable = new Runnable() {

            @Override
            public void run() {

                //TODO - process with update timer for new 30 sec
                Log.e("aleesha","5 sec timer");
                updateLocation(3,context);

                mHandler.postDelayed(this, timer);

            }
        };

        mHandler.postDelayed(mRunnable, timer);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Toast.makeText(this, "Service Destroyed", Toast.LENGTH_LONG).show();
    }


    public void updateLocation(int status,Context context) {

        jsonApiInterface = RetrofitClient.getClient().create(JsonApiInterface.class);
        gpsTracker = new GPSTracker(context);

        String driverId = SharedPrefrences.getInstance().getUserObject().getDriverId();
        String rideID = SharedPrefrences.getString("riderID", context);

        Call<Updatelocation> response = jsonApiInterface.updateLocation(driverId, gpsTracker.getLatitude(), gpsTracker.getLatitude(), rideID, status);
        response.enqueue(new Callback<Updatelocation>() {
            @Override
            public void onResponse(Call<Updatelocation> call, Response<Updatelocation> response) {

                Log.e("aleesha","loc is updating with status 2");
            }

            @Override
            public void onFailure(Call<Updatelocation> call, Throwable t) {

                Log.e("aleesha","loc is failing with status 2");

            }
        });
    }
}
