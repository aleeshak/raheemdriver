package taxi.raheemdriver.com.raheemdriver.ui.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.johnpersano.supertoasts.library.Style;
import com.github.johnpersano.supertoasts.library.SuperActivityToast;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import taxi.raheemdriver.com.raheemdriver.R;
import taxi.raheemdriver.com.raheemdriver.models.CaptainArrived;
import taxi.raheemdriver.com.raheemdriver.models.CollectFee;
import taxi.raheemdriver.com.raheemdriver.models.EndRide;
import taxi.raheemdriver.com.raheemdriver.models.Status;
import taxi.raheemdriver.com.raheemdriver.models.Updatelocation;
import taxi.raheemdriver.com.raheemdriver.networking.JsonApiInterface;
import taxi.raheemdriver.com.raheemdriver.networking.RetrofitClient;
import taxi.raheemdriver.com.raheemdriver.services.ServiceWithStatusThree;
import taxi.raheemdriver.com.raheemdriver.services.ServiceWithStatusTwo;
import taxi.raheemdriver.com.raheemdriver.sharedprefrences.SharedPrefrences;
import taxi.raheemdriver.com.raheemdriver.ui.activities.CollectFeeActivity;
import taxi.raheemdriver.com.raheemdriver.ui.activities.MainActivity;
import taxi.raheemdriver.com.raheemdriver.ui.activities.StartRideActivity;
import taxi.raheemdriver.com.raheemdriver.broadcastreceiver.BroadCastReceiverThree;
import taxi.raheemdriver.com.raheemdriver.broadcastreceiver.BroadCastReceiverTwo;
import taxi.raheemdriver.com.raheemdriver.util.GPSTracker;
import taxi.raheemdriver.com.raheemdriver.util.InternetConnection;
import taxi.raheemdriver.com.raheemdriver.util.Utility;

/**
 * Created by Aleesha Kanwal on 16/08/2018.
 */

public class FragmentMap extends Fragment implements OnMapReadyCallback {

    private GoogleMap mMap;
    JsonApiInterface jsonApiInterface;
    ProgressDialog progressDialog;
    LinearLayout linearLayoutGo, linearLayoutGoPlus, linearLayoutBike, linearLayoutRik;
    Marker mkr;
    GPSTracker gpsTracker;
    double latitude, longitude;
    static private FragmentMap _instance;
    LinearLayout vehicles;
    TextView statusTv;
    BottomSheetBehavior bottomSheetBehavior;
    CoordinatorLayout layout;
    int toggle = 0;
    AppCompatButton details;
    LinearLayout statusLayout;
    TextView address;
    AppCompatButton cancel, arrivedBtn,closeBtnBottomSheet;
    LinearLayout buttonView;
    RelativeLayout addressLayout;
    static Timer timer;
    static Timer startRideTimer,endRideTimer;
    private static final int PERMISSION_REQUEST_CODE = 1;
    boolean isGpsTrue;
    public static final int PERMISSION_REQUEST_CODE_LOCATION = 1;
    int toggleArrivedStart = 0;
    TextView driverName,time,from,to,jobInfo,carType;
    ImageView callBS;
    BroadCastReceiverThree broadCastReceiverThree = new BroadCastReceiverThree();
    BroadCastReceiverTwo broadCastReceiverTwo = new BroadCastReceiverTwo();


  /*  public static FragmentMap getInstance() {
        if (_instance == null) {
            synchronized (FragmentMap.class) {
                if (_instance == null)
                    _instance = new FragmentMap();
            }
        }
        return _instance;
    }*/


    public void goOffline() {
        captainStatus(2);
        vehicles.setBackgroundResource(R.color.red);
        statusTv.setText("You are offline");
        statusTv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.gooffline, 0, 0, 0);
    }

    public void goOnline() {
        captainStatus(1);
        vehicles.setBackgroundResource(R.color.forest_green);
        statusTv.setText("You are online");
        statusTv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.goonline, 0, 0, 0);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home_new, null);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        vehicles = (LinearLayout) rootView.findViewById(R.id.statusLayout);
        statusTv = (TextView) rootView.findViewById(R.id.status);
        View bottomSheet = rootView.findViewById(R.id.bottom_sheet);
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        details = (AppCompatButton) rootView.findViewById(R.id.detailsBtn);
        layout = (CoordinatorLayout) rootView.findViewById(R.id.layout);
        statusLayout = (LinearLayout) rootView.findViewById(R.id.statusLayout);
        buttonView = (LinearLayout) rootView.findViewById(R.id.buttonView);
        address = (TextView) rootView.findViewById(R.id.address);
        cancel = (AppCompatButton) rootView.findViewById(R.id.cancel);
        arrivedBtn = (AppCompatButton) rootView.findViewById(R.id.arrivedBtn);
        addressLayout = (RelativeLayout) rootView.findViewById(R.id.addressLayout);
        closeBtnBottomSheet = (AppCompatButton) rootView.findViewById(R.id.closeBtnBottomSheet);
        callBS = (ImageView) rootView.findViewById(R.id.callBS);

        driverName = (TextView) rootView.findViewById(R.id.driverName);
        time = (TextView) rootView.findViewById(R.id.time);
        from = (TextView) rootView.findViewById(R.id.from);
        to = (TextView)rootView.findViewById(R.id.to);
        jobInfo = (TextView) rootView.findViewById(R.id.jobInfo);
        carType = (TextView) rootView.findViewById(R.id.carType);

        String driver = SharedPrefrences.getString("name",getContext());
        String tim = SharedPrefrences.getString("time",getContext());
        String fromloc = SharedPrefrences.getString("from_loc",getContext());
        String toloc = SharedPrefrences.getString("to_loc",getContext());
        final String mobile = SharedPrefrences.getString("mobile",getContext());

        driverName.setText(driver);
        time.setText(tim);
        from.setText(fromloc);
        to.setText(toloc);

        Log.e("aleesha","called");

        jsonApiInterface = RetrofitClient.getClient().create(JsonApiInterface.class);

        gpsTracker = new GPSTracker(getContext());

        //Handling movement of bottom sheets from buttons
        details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (toggle == 0) {
                    toggle = 1;
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else if (toggle == 1) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                    toggle = 0;
                }
            }
        });

        closeBtnBottomSheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        });


        callBS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String uri = "tel:" + mobile.trim() ;

                if (ActivityCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                    //Creating intents for making a call
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse(uri));
                    getActivity().startActivity(callIntent);

                }else{
                    SuperActivityToast.create(getActivity(), new Style(), Style.TYPE_BUTTON)
                            .setProgressBarColor(Color.WHITE)
                            .setText("You don't assign permission.")
                            .setDuration(Style.DURATION_LONG)
                            .setFrame(Style.FRAME_LOLLIPOP)
                            .setColor(getResources().getColor(R.color.colorPrimary))
                            .setAnimations(Style.ANIMATIONS_POP).show();
                }
            }
        });

        arrivedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (SharedPrefrences.getBoolean("rideAccept", getActivity())) {
                    captainArrivedForPickUp(1);
                } else if (SharedPrefrences.getBoolean("startRide", getActivity())) {
                    if (!SharedPrefrences.getBoolean("driverConfirmed", getActivity())) {
                        Intent intent = new Intent(getContext(), StartRideActivity.class);
                        startActivity(intent);
                    } else {

                      /*  timer.cancel();
                        updateLocation(1);
                        toggleArrivedStart = 2;
                        SharedPrefrences.putBoolean("driverConfirmed", false, getContext());
                        Intent intent = new Intent(getContext(), MainActivity.class);
                        startActivity(intent);
                        startRideTimer.schedule(new TimerTask() {

                            @Override
                            public void run() {
                                getActivity().runOnUiThread(new Runnable() {
                                    public void run() {
                                        updateLocation(2);
                                    }
                                });
                            }
                        }, 0, 10000);*/

                    }
                } else if (SharedPrefrences.getBoolean("endRide", getActivity())) {
                    if(startRideTimer != null) {
                        startRideTimer.cancel();
                        startRideTimer.purge();
                    }
                    endRide();
                }



            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                cancelRide();
            }
        });



     /*   //Handling movement of bottom sheets from sliding
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    heading.setText("Collapsed");
                    heading.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.colorAccent));
                } else if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                    heading.setText("Welcome");
                    heading.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary));
                }
            }

            @Override
            public void onSlide(View bottomSheet, float slideOffset) {
            }
        });*/

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        if(SharedPrefrences.getBoolean("loginfirst",getContext())){
            SharedPrefrences.putBoolean("loginfirst",false,getContext());
            captainStatus(1);
        }

        return rootView;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
            }
        } else {
            //load your map here
            mMap.setMyLocationEnabled(true);
        }


        if (gpsTracker.canGetLocation()) {

            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
            addLocationMarker();
        } else {
            // Can't get location.
            // GPS or network is not enabled.
            // Ask user to enable GPS/network in settings.
            showSettingsAlert();
            addLocationMarker();

        }

        if (SharedPrefrences.getBoolean("rideAccept", getActivity())) {
            setupRideAcceptView();
        } else if (SharedPrefrences.getBoolean("startRide", getActivity())) {
            setupStartRideView();
        } else if (SharedPrefrences.getBoolean("endRide", getActivity())) {
            setupEndRideView();
        }
        else {

            //Testing out comment
            arrivedBtn.setEnabled(false);
            cancel.setEnabled(false);
            details.setEnabled(false);

            addressLayout.setVisibility(View.GONE);
            buttonView.setVisibility(View.GONE);
            statusLayout.setVisibility(View.VISIBLE);
        }
    }

    public void addLocationMarker() {

        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();

        LatLng currentLocation = new LatLng(latitude, longitude);

        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .title("You")
                //.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.raheemmap)));

        mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 16.5f));
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setScrollGesturesEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);

    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

        // Setting Dialog Title
        alertDialog.setTitle("GPS settings");

        // Setting Dialog Message
        alertDialog.setMessage("Raheem would like to use your current location and GPS is not enabled. Do you want to go to settings menu?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);

                isGpsTrue = true;

                //  turnGPSOn();
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

                isGpsTrue = false;
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    private void turnGPSOn() {
        String provider = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if (!provider.contains("gps")) { //if gps is disabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            getActivity().sendBroadcast(poke);
        }
    }

    private void turnGPSOff() {
        String provider = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if (provider.contains("gps")) { //if gps is enabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            getActivity().sendBroadcast(poke);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay!
                    // load your map here also

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    public void captainStatus(int status) {

        Utility.onServiceSuccessNetworkCheck(getActivity(), InternetConnection.isNetworkAvailable(getActivity()));

        String driverId = SharedPrefrences.getInstance().getUserObject().getDriverId();

        Call<Status> response = jsonApiInterface.statusCaptain(status, driverId, gpsTracker.getLatitude(), gpsTracker.getLongitude());
        response.enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {
                Status userList = response.body();

                SuperActivityToast.create(getContext(), new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText(userList.getMessage())
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setAnimations(Style.ANIMATIONS_POP).show();
            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {

                SuperActivityToast.create(getContext(), new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText(t.getMessage())
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setAnimations(Style.ANIMATIONS_POP).show();
                call.cancel();
            }
        });
    }

    private void setupRideAcceptView() {

        addressLayout.setVisibility(View.VISIBLE);
        statusLayout.setVisibility(View.GONE);
        buttonView.setVisibility(View.VISIBLE);
        arrivedBtn.setEnabled(true);
        details.setEnabled(true);
        cancel.setEnabled(true);

    /*    if (toggleArrivedStart == 0) {
            address.setText(SharedPrefrences.getString("from_loc", getActivity()));
        } else if (toggleArrivedStart == 1) {
            address.setText(SharedPrefrences.getString("to_loc", getActivity()));
        }*/


        if (SharedPrefrences.getBoolean("rideAccept", getActivity())) {
            address.setText(SharedPrefrences.getString("from_loc", getActivity()));
        } else if (SharedPrefrences.getBoolean("startRide", getActivity())) {
            address.setText(SharedPrefrences.getString("to_loc", getActivity()));
        }


        cancel.setVisibility(View.GONE);
        timer = new Timer();
        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        updateLocation(3);
                        getActivity().startService(new Intent(getContext(), ServiceWithStatusThree.class));
                    }
                });
            }
        }, 0, 10000);

    }

    public void updateLocation(int status) {

        Log.e("status value",String.valueOf(status));
        Utility.onServiceSuccessNetworkCheck(getActivity(), InternetConnection.isNetworkAvailable(getActivity()));

        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();

        Log.e("lat",String.valueOf(latitude));
        Log.e("lng",String.valueOf(longitude));

        String driverId = SharedPrefrences.getInstance().getUserObject().getDriverId();
        String rideID = SharedPrefrences.getString("riderID", getActivity());

        Call<Updatelocation> response = jsonApiInterface.updateLocation(driverId, latitude, longitude, rideID, status);
        response.enqueue(new Callback<Updatelocation>() {
            @Override
            public void onResponse(Call<Updatelocation> call, Response<Updatelocation> response) {
                Updatelocation updatelocation = response.body();

                mMap.clear();

                addLocationMarker();

                String lat = SharedPrefrences.getString("lat", getActivity());
                String lng = SharedPrefrences.getString("lng", getActivity());

                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(Float.valueOf(lat), Float.valueOf(lng)))
                        .title("Passenger")
                        //.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon)));
            }

            @Override
            public void onFailure(Call<Updatelocation> call, Throwable t) {

                SuperActivityToast.create(getContext(), new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText(t.getMessage())
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setAnimations(Style.ANIMATIONS_POP).show();
                call.cancel();
            }
        });
    }

    @Override
    public void onDestroyView() {
        timer.cancel();
        startRideTimer.cancel();
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        timer.cancel();
        startRideTimer.cancel();
        super.onDetach();
    }

    public void captainArrivedForPickUp(int status) {

        Utility.onServiceSuccessNetworkCheck(getActivity(), InternetConnection.isNetworkAvailable(getActivity()));

        String rideID = SharedPrefrences.getString("riderID", getActivity());
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        Call<CaptainArrived> response = jsonApiInterface.captainArrived(status, rideID);
        response.enqueue(new Callback<CaptainArrived>() {
            @Override
            public void onResponse(Call<CaptainArrived> call, Response<CaptainArrived> response) {
                CaptainArrived userList = response.body();
                progressDialog.cancel();

                SharedPrefrences.putBoolean("startRide", true,getContext());
                SharedPrefrences.putBoolean("endRide", false, getContext());
                SharedPrefrences.putBoolean("driverConfirmed",false,getContext());
                SharedPrefrences.putBoolean("rideAccept",false,getContext());


                Intent intent = new Intent(getContext(), MainActivity.class);
                startActivity(intent);

                SuperActivityToast.create(getContext(), new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText(userList.getMessage())
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setAnimations(Style.ANIMATIONS_POP).show();
            }

            @Override
            public void onFailure(Call<CaptainArrived> call, Throwable t) {
                progressDialog.cancel();
                SuperActivityToast.create(getContext(), new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText(t.getMessage())
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setAnimations(Style.ANIMATIONS_POP).show();
                call.cancel();
            }
        });
    }

    private void dropOffLocation(){

        double lat = Double.parseDouble(SharedPrefrences.getString("lat",getContext()));
        double lng = Double.parseDouble(SharedPrefrences.getString("lng",getContext()));

        if(lat != 0.0 && lng != 0.0) {

            LatLng currentLocation = new LatLng(lat, lng);
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(lat, lng))
                    .title("User")
                    //.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.raheemmap)));

            mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 16.5f));
            mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.getUiSettings().setScrollGesturesEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.getUiSettings().setCompassEnabled(true);
            mMap.getUiSettings().setZoomGesturesEnabled(true);
        }

    }

    private void driverLocationUpdate(){

        endRideTimer = new Timer();
        endRideTimer.schedule(new TimerTask() {

            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {

                        double lat = gpsTracker.getLatitude();
                        double lng = gpsTracker.getLongitude();

                        if(lat != 0.0 && lng != 0.0) {

                            mMap.clear();
                            LatLng currentLocation = new LatLng(lat, lng);
                            mMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(lat, lng))
                                    .title("You")
                                    //.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.raheemmap)));

                            mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation));
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 16.5f));
                            mMap.getUiSettings().setZoomControlsEnabled(true);
                            mMap.getUiSettings().setScrollGesturesEnabled(true);
                            mMap.getUiSettings().setMyLocationButtonEnabled(true);
                            mMap.getUiSettings().setCompassEnabled(true);
                            mMap.getUiSettings().setZoomGesturesEnabled(true);
                        }

                    }
                });
            }
        }, 0, 10000);

    }

    private void setupEndRideView() {

        if(timer != null) {
            timer.cancel();
            timer.purge();
        }

        mMap.clear();
        dropOffLocation();
        driverLocationUpdate();

        updateLocation(1);
        toggleArrivedStart = 2;
        startRideTimer = new Timer();
        startRideTimer.schedule(new TimerTask() {

            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {

                        getActivity().stopService(new Intent(getContext(), ServiceWithStatusThree.class));
                        updateLocation(2);
                        getActivity().startService(new Intent(getContext(), ServiceWithStatusTwo.class));
                    }
                });
            }
        }, 0, 10000);

        addressLayout.setVisibility(View.GONE);
        statusLayout.setVisibility(View.GONE);
        buttonView.setVisibility(View.VISIBLE);
        arrivedBtn.setEnabled(true);
        details.setEnabled(true);
        arrivedBtn.setBackgroundResource(R.drawable.end);
    }

    private void setupStartRideView() {

        addressLayout.setVisibility(View.VISIBLE);
        statusLayout.setVisibility(View.GONE);
        buttonView.setVisibility(View.VISIBLE);
        arrivedBtn.setEnabled(true);
        details.setEnabled(true);
        cancel.setEnabled(false);
        arrivedBtn.setBackgroundResource(R.drawable.start);


        new CountDownTimer(300000, 1000) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                cancel.setEnabled(true);
                cancel.setBackgroundResource(R.drawable.cancelred);
            }
        }.start();

        new CountDownTimer(600000, 1000) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished) {
                address.setText("Waiting Time " + String.format("%d : %d ",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {

            }
        }.start();

    }

    public void endRide() {

        Utility.onServiceSuccessNetworkCheck(getActivity(), InternetConnection.isNetworkAvailable(getActivity()));

        getActivity().stopService(new Intent(getContext(), ServiceWithStatusTwo.class));

        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();

        String rideID = SharedPrefrences.getString("riderID", getActivity());
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        Call<EndRide> response = jsonApiInterface.endRide(rideID, latitude, longitude, SharedPrefrences.getInstance().getUserObject().getVehicleId());
        response.enqueue(new Callback<EndRide>() {
            @Override
            public void onResponse(Call<EndRide> call, Response<EndRide> response) {
                EndRide userList = response.body();
                progressDialog.cancel();

                SharedPrefrences.putBoolean("startRide", false,getContext());
                SharedPrefrences.putBoolean("endRide", false, getContext());
                SharedPrefrences.putBoolean("driverConfirmed",false,getContext());
                SharedPrefrences.putBoolean("rideAccept",false,getContext());
                SharedPrefrences.getInstance().setFairObject(userList.getObject(), getContext());


                if(endRideTimer != null) {
                    endRideTimer.cancel();
                    endRideTimer.purge();
                }

                Intent intent = new Intent(getContext(), CollectFeeActivity.class);
                startActivity(intent);

                SuperActivityToast.create(getContext(), new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText(userList.getMessage())
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setAnimations(Style.ANIMATIONS_POP).show();
                captainStatus(1);



            }

            @Override
            public void onFailure(Call<EndRide> call, Throwable t) {
                progressDialog.cancel();
                SuperActivityToast.create(getContext(), new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText(t.getMessage())
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setAnimations(Style.ANIMATIONS_POP).show();
                call.cancel();
            }
        });
    }


    public void cancelRide() {

        Utility.onServiceSuccessNetworkCheck(getActivity(), InternetConnection.isNetworkAvailable(getContext()));

        String rideID = SharedPrefrences.getString("riderID", getContext());
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        Call<CollectFee> response = jsonApiInterface.cancelRide(rideID);
        response.enqueue(new Callback<CollectFee>() {
            @Override
            public void onResponse(Call<CollectFee> call, Response<CollectFee> response) {
                CollectFee userList = response.body();
                progressDialog.cancel();


                SharedPrefrences.putBoolean("startRide", false, getContext());
                SharedPrefrences.putBoolean("rideAccept", false, getContext());
                SharedPrefrences.putBoolean("endRide", false, getContext());
                SharedPrefrences.putBoolean("driverConfirmed",false,getContext());

                Intent intent = new Intent(getContext(), MainActivity.class);
                startActivity(intent);
                SuperActivityToast.create(getContext(), new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText(userList.getMessage())
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setAnimations(Style.ANIMATIONS_POP).show();

                captainStatus(1);
            }

            @Override
            public void onFailure(Call<CollectFee> call, Throwable t) {
                progressDialog.cancel();
                SuperActivityToast.create(getContext(), new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText(t.getMessage())
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setAnimations(Style.ANIMATIONS_POP).show();
                call.cancel();
            }
        });
    }
}
