package taxi.raheemdriver.com.raheemdriver.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.github.johnpersano.supertoasts.library.Style;
import com.github.johnpersano.supertoasts.library.SuperActivityToast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import taxi.raheemdriver.com.raheemdriver.R;
import taxi.raheemdriver.com.raheemdriver.models.CollectFee;
import taxi.raheemdriver.com.raheemdriver.networking.JsonApiInterface;
import taxi.raheemdriver.com.raheemdriver.networking.RetrofitClient;
import taxi.raheemdriver.com.raheemdriver.sharedprefrences.SharedPrefrences;
import taxi.raheemdriver.com.raheemdriver.util.InternetConnection;
import taxi.raheemdriver.com.raheemdriver.util.Utility;

/**
 * Created by Aleesha Kanwal on 18/10/2018.
 */
public class CollectFeeActivity extends AppCompatActivity {

    AppCompatButton collect;
    ProgressDialog progressDialog;
    JsonApiInterface jsonApiInterface;
    EditText input_amount;
    TextView distance,time,pay,balanceBefore,balanceAccount,rideAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collect_fee);

        jsonApiInterface = RetrofitClient.getClient().create(JsonApiInterface.class);
        collect = (AppCompatButton) findViewById(R.id.collect);
        input_amount = (EditText) findViewById(R.id.input_amount);
        distance = (TextView) findViewById(R.id.distance);
        time = (TextView) findViewById(R.id.time);
        pay = (TextView) findViewById(R.id.pay);
        balanceBefore = (TextView) findViewById(R.id.balanceBefore);
        balanceAccount = (TextView) findViewById(R.id.balanceAccount);
        rideAmount = (TextView) findViewById(R.id.rideAmount);
        rideAmount.setText("Your Ride Amount is PKR = " + SharedPrefrences.getInstance().getFairObject().getRideAmt() );

        distance.setText("Total Distance = " + SharedPrefrences.getInstance().getFairObject().getDistance());
        time.setText("Time Taken =" + SharedPrefrences.getInstance().getFairObject().getTime());
        pay.setText("Pay Amount is = " + SharedPrefrences.getInstance().getFairObject().getPayAmount());
        balanceAccount.setText("Balance Before Ride = " + SharedPrefrences.getInstance().getFairObject().getBalBefore());
        balanceBefore.setText("Balance in Account After Ride = " + SharedPrefrences.getInstance().getFairObject().getBalAfter());

        collect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                collectFee();
            }
        });


    }


    public void collectFee() {

        Utility.onServiceSuccessNetworkCheck(this, InternetConnection.isNetworkAvailable(this));

        String rideID = SharedPrefrences.getString("riderID", this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        Call<CollectFee> response = jsonApiInterface.collectFee(rideID,SharedPrefrences.getInstance().getFairObject().getPayAmount(),input_amount.getText().toString());
        response.enqueue(new Callback<CollectFee>() {
            @Override
            public void onResponse(Call<CollectFee> call, Response<CollectFee> response) {
                CollectFee userList = response.body();
                progressDialog.cancel();


                SharedPrefrences.putBoolean("startRide",false,getApplicationContext());
                SharedPrefrences.putBoolean("rideAccept",false,getApplicationContext());

                Intent intent = new Intent(getApplicationContext(), RateActivity.class);
                startActivity(intent);
                SuperActivityToast.create(CollectFeeActivity.this, new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText(userList.getMessage())
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setAnimations(Style.ANIMATIONS_POP).show();
            }

            @Override
            public void onFailure(Call<CollectFee> call, Throwable t) {
                progressDialog.cancel();
                SuperActivityToast.create(CollectFeeActivity.this, new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText(t.getMessage())
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setAnimations(Style.ANIMATIONS_POP).show();
                call.cancel();
            }
        });
    }
}
