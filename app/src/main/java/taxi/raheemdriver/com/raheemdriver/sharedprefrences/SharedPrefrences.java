package taxi.raheemdriver.com.raheemdriver.sharedprefrences;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import taxi.raheemdriver.com.raheemdriver.App;
import taxi.raheemdriver.com.raheemdriver.models.FairObject;
import taxi.raheemdriver.com.raheemdriver.models.Object;

/**
 * Created by Aleesha Kanwal on 19/05/2017.
 */

public class SharedPrefrences {

    SharedPreferences sharedPreferences = null;
    SharedPreferences.Editor prefsEditor;
    private String sharedPreferencesName = "AppPrefs";
    static private SharedPrefrences _instance;

    private String userObject = "userObject";
    private String userObjectList = "userObjectList";
    private String bundle = "bundle";
    private String fairObject = "fairObject";

    private String driverObject = "driverObject";

    public SharedPrefrences(Context appCtx) {
        sharedPreferences =
                appCtx.getSharedPreferences(sharedPreferencesName, Context.MODE_PRIVATE);
        prefsEditor = sharedPreferences.edit();
        prefsEditor.apply();
    }

    public static SharedPrefrences getInstance() {

        if (_instance == null) {
            synchronized (SharedPrefrences.class) {
                if (_instance == null)
                    _instance = new SharedPrefrences(App.getAppContext());
            }
        }
        return _instance;
    }

    public static void putString(String key, String value, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void putInt(String key, int value, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }


    public static void putBoolean(String key, Boolean value, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);

        editor.commit();
    }

    public static void putFloat(String key, float value, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putFloat(key, value);

        editor.commit();
    }


    public static void putLong(String key, long value, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(key, value);

        editor.commit();
    }

    public static String getString(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, null);
    }

    public static int getInt(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getInt(key, 10);
    }

    public static Boolean getBoolean(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(key, false);
    }

    public static Float getFloat(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getFloat(key, 0);
    }


    public static long getLong(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getLong(key, 0L);
    }

    public static void removeKey(String key, Context context) {

        SharedPreferences mySPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = mySPrefs.edit();
        editor.remove(key);
        editor.apply();

    }

    public  ArrayList<Object> getUserObjectList() {
        Gson gson = new Gson();
        String jsonString = sharedPreferences.getString(userObjectList, "");
        if (jsonString.equals("")) {
            return new ArrayList<>();
        } else {
            return gson.fromJson(jsonString, new TypeToken<ArrayList<Object>>() {
            }.getType());
        }
    }

    public  void setUserObjectList(ArrayList<Object> legs,Context context) {
        Gson gson = new Gson();
        String jsonString = gson.toJson(legs);
        prefsEditor.putString(userObjectList, jsonString);
        prefsEditor.apply();
    }


    public Object getUserObject() {
        Gson gson = new Gson();
        String jsonString = sharedPreferences.getString(userObject, "");
        if (!jsonString.equals("")) {
            try {
                return gson.fromJson(jsonString, Object.class);

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    public void setFairObject(FairObject data, Context context) {

        if (data != null) {
            Gson gson = new Gson();
            String jsonString = gson.toJson(data);
            prefsEditor.putString(fairObject, jsonString);
            prefsEditor.apply();
        } else {
            prefsEditor.remove(fairObject); // pass null to clear data
        }
    }

    public FairObject getFairObject() {
        Gson gson = new Gson();
        String jsonString = sharedPreferences.getString(fairObject, "");
        if (!jsonString.equals("")) {
            try {
                return gson.fromJson(jsonString, FairObject.class);

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    public void setUserObject(Object data, Context context) {

        if (data != null) {
            Gson gson = new Gson();
            String jsonString = gson.toJson(data);
            prefsEditor.putString(userObject, jsonString);
            prefsEditor.apply();
        } else {
            prefsEditor.remove(userObject); // pass null to clear data
        }
    }


    public Bundle getBundle() {
        Gson gson = new Gson();
        String jsonString = sharedPreferences.getString(bundle, "");
        if (!jsonString.equals("")) {
            try {
                return gson.fromJson(jsonString, Bundle.class);

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }


    public void setBundle(Bundle data, Context context) {

        if (data != null) {
            Gson gson = new Gson();
            String jsonString = gson.toJson(data);
            prefsEditor.putString(bundle, jsonString);
            prefsEditor.apply();
        } else {
            prefsEditor.remove(bundle); // pass null to clear data
        }
    }
}
