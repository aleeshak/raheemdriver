package taxi.raheemdriver.com.raheemdriver.ui.interfaces;

import android.view.View;

/**
 * Created by Aleesha Kanwal on 21/08/2018.
 */
public interface ClickListenerAdapter {

    void onClick(View view, int position);
}
