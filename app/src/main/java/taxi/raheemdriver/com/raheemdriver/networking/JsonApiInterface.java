package taxi.raheemdriver.com.raheemdriver.networking;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import taxi.raheemdriver.com.raheemdriver.constants.BackendConstants;
import taxi.raheemdriver.com.raheemdriver.models.CaptainArrived;
import taxi.raheemdriver.com.raheemdriver.models.Captainresponse;
import taxi.raheemdriver.com.raheemdriver.models.CollectFee;
import taxi.raheemdriver.com.raheemdriver.models.EndRide;
import taxi.raheemdriver.com.raheemdriver.models.Login;
import taxi.raheemdriver.com.raheemdriver.models.POJO.Example;
import taxi.raheemdriver.com.raheemdriver.models.Pin;
import taxi.raheemdriver.com.raheemdriver.models.Status;
import taxi.raheemdriver.com.raheemdriver.models.Updatelocation;

/**
 * Created by Aleesha Kanwal
 */

public interface JsonApiInterface {

    @GET("api/directions/json?key=AIzaSyDXDZuqzuntjqZgTGlmTYJAL0VX6BxwpeY")
    Call<Example> getDistanceDuration(@Query("units") String units, @Query("origin") String origin, @Query("destination") String destination, @Query("mode") String mode);

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST(BackendConstants.FORGOT_PASSWORD)
    Call<Pin> forgotPassword(@Field("mobile") String mobile);

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST(BackendConstants.LOGIN_CAPTAIN)
    Call<Login> loginCaptain(@Field("mobile") String mobile,@Field("password") String password,@Field("token") String token);

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST(BackendConstants.STATUS_CAPTAIN)
    Call<Status> statusCaptain(@Field("status") int status, @Field("driver_id") String driver_id, @Field("lat") double lat, @Field("lng") double lng);

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST(BackendConstants.NEW_CAPTAIN_RESPONSE)
    Call<Captainresponse> captainResponse(@Field("ride_id") String  ride_id, @Field("response") int response);

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST(BackendConstants.UPDATE_DRIVER_POSITION)
    Call<Updatelocation> updateLocation(@Field("driver_id") String driver_id, @Field("lat") double lat, @Field("lng") double lng, @Field("ride_id") String ride_id,@Field("status") int status);

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST(BackendConstants.CAPTAIN_ARRIVED)
    Call<CaptainArrived> captainArrived(@Field("status") int  status, @Field("ride_id") String response);

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST(BackendConstants.END_RIDE)
    Call<EndRide> endRide(@Field("ride_id") String  ride_id, @Field("lat") double lat, @Field("lng") double lng, @Field("vehicle_id") String vehicle_id);

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST(BackendConstants.COLLECT_FEE)
    Call<CollectFee> collectFee(@Field("ride_id") String  ride_id, @Field("rent") Integer rent,@Field("amount") String amount);

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST(BackendConstants.RATE_USER)
    Call<CollectFee> rateUser(@Field("ride_id") String  ride_id, @Field("rating") float rating);

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST(BackendConstants.CANCEL_RIDE)
    Call<CollectFee> cancelRide(@Field("ride_id") String  ride_id);

}
