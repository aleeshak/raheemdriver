package taxi.raheemdriver.com.raheemdriver.ui.activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.github.johnpersano.supertoasts.library.Style;
import com.github.johnpersano.supertoasts.library.SuperActivityToast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import taxi.raheemdriver.com.raheemdriver.R;
import taxi.raheemdriver.com.raheemdriver.models.Pin;
import taxi.raheemdriver.com.raheemdriver.networking.JsonApiInterface;
import taxi.raheemdriver.com.raheemdriver.networking.RetrofitClient;
import taxi.raheemdriver.com.raheemdriver.util.InternetConnection;
import taxi.raheemdriver.com.raheemdriver.util.Utility;

/**
 * Created by Aleesha Kanwal on 15/08/2018.
 */

public class ForgotPinActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    JsonApiInterface jsonApiInterface;
    EditText mobileTV;
    String mobile;
    AppCompatButton submitBtn;
    final String EMAIL_PATTERN = "^[a-zA-Z0-9#_~!$&'()*+,;=:.\"(),:;<>@\\[\\]\\\\]+@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*$";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pin);

        jsonApiInterface = RetrofitClient.getClient().create(JsonApiInterface.class);
        mobileTV = (EditText) findViewById(R.id.passwordTV);
        submitBtn = (AppCompatButton) findViewById(R.id.submitBtn);
        submitBtn.setEnabled(false);

        mobileTV.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub

                String mobile = mobileTV.getText().toString().trim();
                if(!Utility.isEmpty(mobile)) {
                    submitBtn.setEnabled(true);
                    submitBtn.setBackgroundResource(R.drawable.submit);
                }

                else {
                    submitBtn.setBackgroundResource(R.drawable.disablesubmit);
                    submitBtn.setEnabled(false);
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
            }
        });



        submitBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                resetPassword();


            }
        });
    }


    public void resetPassword() {
        mobile = mobileTV.getText().toString().trim();
        Utility.onServiceSuccessNetworkCheck(this, InternetConnection.isNetworkAvailable(this));
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        Call<Pin> response = jsonApiInterface.forgotPassword("34234234234");
        response.enqueue(new Callback<Pin>() {
            @Override
            public void onResponse(Call<Pin> call, Response<Pin> response) {
                Pin serverResponse = response.body();

                progressDialog.dismiss();

                SuperActivityToast.create(ForgotPinActivity.this, new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText(serverResponse.getMessage())
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setAnimations(Style.ANIMATIONS_POP).show();
            }

            @Override
            public void onFailure(Call<Pin> call, Throwable t) {
                t.getMessage();
                progressDialog.cancel();

                SuperActivityToast.create(ForgotPinActivity.this, new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText(t.getMessage())
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setAnimations(Style.ANIMATIONS_POP).show();

                call.cancel();
            }
        });
    }
}
