package taxi.raheemdriver.com.raheemdriver.broadcastreceiver;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import taxi.raheemdriver.com.raheemdriver.models.Updatelocation;
import taxi.raheemdriver.com.raheemdriver.networking.JsonApiInterface;
import taxi.raheemdriver.com.raheemdriver.networking.RetrofitClient;
import taxi.raheemdriver.com.raheemdriver.sharedprefrences.SharedPrefrences;
import taxi.raheemdriver.com.raheemdriver.util.GPSTracker;
import taxi.raheemdriver.com.raheemdriver.util.InternetConnection;
import taxi.raheemdriver.com.raheemdriver.util.Utility;

/**
 * Created by Aleesha Kanwal on 16/11/2018.
 */
public class BroadCastReceiverTwo extends BroadcastReceiver {


    JsonApiInterface jsonApiInterface;
    GPSTracker gpsTracker;


    @Override
    public void onReceive(Context context, Intent intent) {
        jsonApiInterface = RetrofitClient.getClient().create(JsonApiInterface.class);
        updateLocation(2,context);
    }


    public void updateLocation(int status,Context context) {

        Utility.onServiceSuccessNetworkCheck((Activity)context, InternetConnection.isNetworkAvailable(context));
        String driverId = SharedPrefrences.getInstance().getUserObject().getDriverId();
        String rideID = SharedPrefrences.getString("riderID", context);

        Call<Updatelocation> response = jsonApiInterface.updateLocation(driverId,   gpsTracker.getLatitude(), gpsTracker.getLatitude(), rideID, status);
        response.enqueue(new Callback<Updatelocation>() {
            @Override
            public void onResponse(Call<Updatelocation> call, Response<Updatelocation> response) {

                Log.e("aleesha","loc is updating with status 2");
            }

            @Override
            public void onFailure(Call<Updatelocation> call, Throwable t) {

                Log.e("aleesha","loc is failing with status 2");

            }
        });
    }

}
