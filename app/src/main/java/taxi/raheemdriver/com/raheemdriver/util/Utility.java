package taxi.raheemdriver.com.raheemdriver.util;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import taxi.raheemdriver.com.raheemdriver.R;


/**
 * Created by Aleesha Kanwal on 28/07/2018.
 */

public class Utility {
    
    public static String getAndroidID(@NonNull Context context) {
        String android_id= Settings.System.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return android_id;
    }

    private Date stringToDate(String aDate, String aFormat) {

        if(aDate==null) return null;
        ParsePosition pos = new ParsePosition(0);
        SimpleDateFormat simpledateformat = new SimpleDateFormat(aFormat);
        Date stringDate = simpledateformat.parse(aDate, pos);
        return stringDate;

    }

    public static void hideKeyboardFrom(View view, Context context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public static String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream baos=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
        byte [] b=baos.toByteArray();
        String temp= Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    public static Bitmap StringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    public static File getFileFromBitmapString(String filename) {
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        OutputStream outStream = null;

        File file = new File(filename + ".png");
        if (file.exists()) {
            file.delete();
            file = new File(extStorageDirectory, filename + ".png");
            Log.e("file exist", "" + file + ",Bitmap= " + filename);
        }
        try {
            // make a new bitmap from your file
            Bitmap bitmap = BitmapFactory.decodeFile(file.getName());

            outStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("file", "" + file);
        return file;

    }

    public static void showOkPopUp(final Activity context, String message) {
        final MessageDialog dialog = new MessageDialog(context, message, R.string
                .dialog_general_ok, -1, -1);
        dialog.setCancelable(false);
        if (!context.isFinishing()) {
            dialog.show();

            dialog.okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.cancel();
                }
            });
        }
    }


    public static void showNetworkFailurePopUp(final Activity context) {
        final MessageDialog dialog = new MessageDialog(context, R.string.string_internet_connection_not_available, R.string
                .dialog_general_ok, -1, -1);
        dialog.setCancelable(false);
        if (!context.isFinishing()) {
            dialog.show();

            dialog.okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.cancel();
                }
            });
        }
    }


    public static void showErrorPopUp(final Activity context, String message) {
        final MessageDialog dialog = new MessageDialog(context, message, R.string
                .dialog_general_ok, -1, -1);
        dialog.setCancelable(false);
        if (!context.isFinishing()) {
            dialog.show();

            dialog.okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.cancel();
                }
            });
        }
    }

    public static int getDeviceOsVersion() {
        return android.os.Build.VERSION.SDK_INT;
    }

    public static boolean isEmpty(@Nullable CharSequence str) {
        if (str == null || str.length() == 0)
            return true;
        else
            return false;
    }



    public static void showAlertPopup(final Activity context, String message) {

        final MessageDialog dialog = new MessageDialog(context, message,
                -1, R.string.dialog_edit_no_text, R.string.dialog_edit_yes_text);
        dialog.setCancelable(false);
        if (!context.isFinishing()) {
            dialog.show();
            dialog.yesButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.cancel();

                }
            });

            dialog.noButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.cancel();
                }
            });
        }
    }

    public static void onServiceFailureNetworkCheck(Activity context, String error, boolean isNetworkAvailable) {

        if (!isNetworkAvailable)
            showNetworkFailurePopUp(context);
        else {
            if (error != null && error.equals(""))
                error = context.getString(R.string.error_general);
            showErrorPopUp(context, error);
        }
    }

    public static void onServiceSuccessNetworkCheck(Activity context, boolean isNetworkAvailable) {

        if (!isNetworkAvailable) {
            showNetworkFailurePopUp(context);
            return;
        }
    }


    public static boolean isPhoneNumberValid(String phoneNumber) {
        return isPhoneNumberValidRegx(phoneNumber);
    }

    public static boolean isPhoneNumberValidRegx(String phoneNumber) {
        boolean isValid = true;
       /* if (!phoneNumber.startsWith("+")) {
            isValid = false;
        }*/ /*else *//*if (phoneNumber.lastIndexOf('+') != 0) {
            isValid = false;
        } else */if (!(phoneNumber != null && phoneNumber.length() >= 7 && phoneNumber.length() <= 15)) {
            isValid = false;
        }
        return isValid;
    }

    public static boolean isAppRunning(final Context context, final String packageName) {
        final ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
        if (procInfos != null)
        {
            for (final ActivityManager.RunningAppProcessInfo processInfo : procInfos) {
                if (processInfo.processName.equals(packageName)) {
                    return true;
                }
            }
        }
        return false;
    }


}
