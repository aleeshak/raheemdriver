
package taxi.raheemdriver.com.raheemdriver.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EndRide {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("object")
    @Expose
    private FairObject object;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public FairObject getObject() {
        return object;
    }

    public void setObject(FairObject object) {
        this.object = object;
    }

}
