package taxi.raheemdriver.com.raheemdriver.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;

import com.github.johnpersano.supertoasts.library.Style;
import com.github.johnpersano.supertoasts.library.SuperActivityToast;
import com.google.firebase.FirebaseApp;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import taxi.raheemdriver.com.raheemdriver.R;
import taxi.raheemdriver.com.raheemdriver.constants.Constants;
import taxi.raheemdriver.com.raheemdriver.database.DatabaseHelper;
import taxi.raheemdriver.com.raheemdriver.models.Login;
import taxi.raheemdriver.com.raheemdriver.models.Status;
import taxi.raheemdriver.com.raheemdriver.networking.JsonApiInterface;
import taxi.raheemdriver.com.raheemdriver.networking.RetrofitClient;
import taxi.raheemdriver.com.raheemdriver.sharedprefrences.SharedPrefrences;
import taxi.raheemdriver.com.raheemdriver.util.GPSTracker;
import taxi.raheemdriver.com.raheemdriver.util.InternetConnection;
import taxi.raheemdriver.com.raheemdriver.util.Utility;

/**
 * Created by Aleesha Kanwal on 15/08/2018.
 */

public class LoginActivity extends AppCompatActivity {

    AppCompatButton submit,captain;
    private DatabaseHelper databaseHelper;
    JsonApiInterface jsonApiInterface;


    ProgressDialog progressDialog;
    TextInputLayout input_phone,pinTL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        submit = (AppCompatButton) findViewById(R.id.signin);
        TextView pin = (TextView) findViewById(R.id.pin);
        databaseHelper = new DatabaseHelper(this);
        input_phone = (TextInputLayout) findViewById(R.id.mobile);
        pinTL = (TextInputLayout) findViewById(R.id.pinL);
        captain = (AppCompatButton) findViewById(R.id.captain);
        jsonApiInterface = RetrofitClient.getClient().create(JsonApiInterface.class);


        pin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),ForgotPinActivity.class);
                startActivity(intent);

            }
        });

        captain.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://raheemcar.com/raheem/sign_up"));
                startActivity(browserIntent);

            }
        });

        submit.setEnabled(false);

        pinTL.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub

                if(!Utility.isEmpty( pinTL.getEditText().getText().toString().trim())){
                    submit.setEnabled(true);
                    submit.setBackgroundResource(R.drawable.submit);
                }
                else {
                    submit.setBackgroundResource(R.drawable.disablesubmit);
                    submit.setEnabled(false);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


             loginCaptain();

            }
        });

    }



    public void loginCaptain() {
        /**
         POST name and job Url encoded.
         **/

        if (FirebaseApp.getApps(this).isEmpty()) {
            FirebaseApp.initializeApp(this);
        }

        Utility.onServiceSuccessNetworkCheck(this, InternetConnection.isNetworkAvailable(this));

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        String token;

        token = SharedPrefrences.getString(Constants.FIREBASE_TOKEN,this);

        Call<Login> response = jsonApiInterface.loginCaptain(input_phone.getEditText().getText().toString(),pinTL.getEditText().getText().toString(),token);
        response.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                Login userList = response.body();

                if (userList.getObject() != null) {

                    progressDialog.cancel();
                    SharedPrefrences.putBoolean("login",true,getApplicationContext());
                    SharedPrefrences.putBoolean("loginfirst",true,getApplicationContext());
                    captainStatus(1);
                    SharedPrefrences.getInstance().setUserObject(userList.getObject(),getApplicationContext());
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);

                }
                else {

                    SuperActivityToast.create(LoginActivity.this, new Style(), Style.TYPE_BUTTON)
                            .setProgressBarColor(Color.WHITE)
                            .setText(userList.getMessage())
                            .setDuration(Style.DURATION_LONG)
                            .setFrame(Style.FRAME_LOLLIPOP)
                            .setColor(getResources().getColor(R.color.colorPrimary))
                            .setAnimations(Style.ANIMATIONS_POP).show();

                    progressDialog.cancel();

                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                t.getMessage();
                progressDialog.cancel();

                SuperActivityToast.create(LoginActivity.this, new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText(t.getMessage())
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setAnimations(Style.ANIMATIONS_POP).show();

              //  Utility.onServiceFailureNetworkCheck(LoginActivity.this,t.getMessage(),InternetConnection.isNetworkAvailable(LoginActivity.this));
                call.cancel();
            }
        });
    }

    public void captainStatus(int status) {
        GPSTracker gpsTracker;
        gpsTracker = new GPSTracker(this);


        Utility.onServiceSuccessNetworkCheck(this, InternetConnection.isNetworkAvailable(this));


        String driverId = SharedPrefrences.getInstance().getUserObject().getDriverId();

        Call<Status> response = jsonApiInterface.statusCaptain(status, driverId, gpsTracker.getLatitude(), gpsTracker.getLongitude());
        response.enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {
                Status userList = response.body();

                SuperActivityToast.create(LoginActivity.this, new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText(userList.getMessage())
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setAnimations(Style.ANIMATIONS_POP).show();

            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {

                SuperActivityToast.create(LoginActivity.this, new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText(t.getMessage())
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setAnimations(Style.ANIMATIONS_POP).show();
                call.cancel();
            }
        });
    }
}
