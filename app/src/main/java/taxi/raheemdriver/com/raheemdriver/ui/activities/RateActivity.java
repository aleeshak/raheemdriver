package taxi.raheemdriver.com.raheemdriver.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import com.github.johnpersano.supertoasts.library.Style;
import com.github.johnpersano.supertoasts.library.SuperActivityToast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import taxi.raheemdriver.com.raheemdriver.R;
import taxi.raheemdriver.com.raheemdriver.models.CollectFee;
import taxi.raheemdriver.com.raheemdriver.networking.JsonApiInterface;
import taxi.raheemdriver.com.raheemdriver.networking.RetrofitClient;
import taxi.raheemdriver.com.raheemdriver.sharedprefrences.SharedPrefrences;
import taxi.raheemdriver.com.raheemdriver.util.InternetConnection;
import taxi.raheemdriver.com.raheemdriver.util.Utility;

/**
 * Created by Aleesha Kanwal on 20/10/2018.
 */
public class RateActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    JsonApiInterface jsonApiInterface;
    RatingBar driverRating;
    AppCompatButton finish;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate);
        driverRating = (RatingBar) findViewById(R.id.driverRating);
        finish = (AppCompatButton) findViewById(R.id.finish);
        TextView driverName = (TextView) findViewById(R.id.driverName);

        String driver = SharedPrefrences.getString("name",getApplicationContext());
        driverName.setText("Rate " + driver);


        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rateUser();
            }
        });

        jsonApiInterface = RetrofitClient.getClient().create(JsonApiInterface.class);

    }

    public void rateUser() {

        Utility.onServiceSuccessNetworkCheck(this, InternetConnection.isNetworkAvailable(this));

        String rideID = SharedPrefrences.getString("riderID", this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        Call<CollectFee> response = jsonApiInterface.rateUser(rideID,driverRating.getRating());
        response.enqueue(new Callback<CollectFee>() {
            @Override
            public void onResponse(Call<CollectFee> call, Response<CollectFee> response) {
                CollectFee userList = response.body();
                progressDialog.cancel();


                SharedPrefrences.putBoolean("startRide",false,getApplicationContext());
                SharedPrefrences.putBoolean("rideAccept",false,getApplicationContext());

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                SuperActivityToast.create(RateActivity.this, new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText(userList.getMessage())
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setAnimations(Style.ANIMATIONS_POP).show();
            }

            @Override
            public void onFailure(Call<CollectFee> call, Throwable t) {
                progressDialog.cancel();
                SuperActivityToast.create(RateActivity.this, new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText(t.getMessage())
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setAnimations(Style.ANIMATIONS_POP).show();
                call.cancel();
            }
        });
    }
}
