package taxi.raheemdriver.com.raheemdriver.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.TextView;

import taxi.raheemdriver.com.raheemdriver.R;
import taxi.raheemdriver.com.raheemdriver.sharedprefrences.SharedPrefrences;

/**
 * Created by Aleesha Kanwal on 18/10/2018.
 */
public class StartRideActivity extends AppCompatActivity {

    AppCompatButton start,close;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_ride);

        start = (AppCompatButton) findViewById(R.id.start);
        close = (AppCompatButton) findViewById(R.id.close);
        TextView driverName = (TextView) findViewById(R.id.driverName);

        String driver = SharedPrefrences.getString("name",getApplicationContext());
        driverName.setText(driver);

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPrefrences.putBoolean("driverConfirmed",true,getApplicationContext());
                SharedPrefrences.putBoolean("rideAccept", false, getApplicationContext());
                SharedPrefrences.putBoolean("startRide", false, getApplicationContext());
                SharedPrefrences.putBoolean("endRide", true, getApplicationContext());

                Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPrefrences.putBoolean("startRide", true,getApplicationContext());
                SharedPrefrences.putBoolean("endRide", false, getApplicationContext());
                SharedPrefrences.putBoolean("driverConfirmed",false,getApplicationContext());
                SharedPrefrences.putBoolean("rideAccept",false,getApplicationContext());

                Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);
            }
        });

    }

}