
package taxi.raheemdriver.com.raheemdriver.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pin {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("new_password")
    @Expose
    private Integer newPassword;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(Integer newPassword) {
        this.newPassword = newPassword;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
