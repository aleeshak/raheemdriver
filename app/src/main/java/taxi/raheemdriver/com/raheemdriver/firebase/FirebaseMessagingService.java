package taxi.raheemdriver.com.raheemdriver.firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import taxi.raheemdriver.com.raheemdriver.R;
import taxi.raheemdriver.com.raheemdriver.sharedprefrences.SharedPrefrences;
import taxi.raheemdriver.com.raheemdriver.ui.activities.RideNotificationActivity;
import taxi.raheemdriver.com.raheemdriver.util.Utility;

/**
 * Created by filipp on 5/23/2016.
 */
public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService{
    public static int NOTIFICATION_ID = 1;
    public NotificationManager notificationManager;
    public int num = ++NOTIFICATION_ID;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        sendNotification(remoteMessage.getData());


    }

    private void sendNotification(Map<String, String> data) {


        final Bundle msg = new Bundle();
        long delayInMilliseconds = 5000;

        for (String key : data.keySet()) {
            Log.e("sadeeq", data.get(key));
            msg.putString(key, data.get(key));
        }


        Intent intent = new Intent(this, RideNotificationActivity.class);
        if (msg.containsKey("action")) {
            intent.putExtra("action", msg.getString("action"));
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        final PendingIntent pendingIntent = PendingIntent.getActivity(this, num , intent,
                PendingIntent.FLAG_ONE_SHOT);

        final Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
       NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle(msg.getString("title"))
                .setContentText(msg.getString("message"))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

/*        SessionManager sessionManager = new SessionManager(FirebaseMessagingService.this);
        sessionManager.setUserInformation(msg.getString("from_loc"),msg.getString("to_loc"),msg.getString("ride_id"),
                msg.getString("name"),msg.getString("mobile"),msg.getString("lat"),
                msg.getString("lng"),msg.getString("time"));*/
        notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);



        Log.e("name", msg.getString("name"));
        Log.e("riderID", msg.getString("ride_id"));
        Log.e("mobile", msg.getString("mobile"));
        Log.e("lat", msg.getString("lat"));
        Log.e("lng", msg.getString("lng"));
        Log.e("from_loc", msg.getString("from_loc"));
        Log.e("to_loc", msg.getString("to_loc"));
        Log.e("time", msg.getString("time"));

        SharedPrefrences.putString("name", msg.getString("name"),getApplicationContext());
        SharedPrefrences.putString("riderID", msg.getString("ride_id"),getApplicationContext());
        SharedPrefrences.putString("mobile", msg.getString("mobile"),getApplicationContext());
        SharedPrefrences.putString("lat", msg.getString("drop_lat"),getApplicationContext());
        SharedPrefrences.putString("lng", msg.getString("drop_lng"),getApplicationContext());
        SharedPrefrences.putString("from_loc", msg.getString("from_loc"),getApplicationContext());
        SharedPrefrences.putString("to_loc", msg.getString("to_loc"),getApplicationContext());
        SharedPrefrences.putString("time", msg.getString("time"),getApplicationContext());


        //SharedPrefrences.getInstance().setBundle(msg,getApplicationContext());
     /*   SharedPrefrences.putString("msg",msg.toString(),getApplicationContext());*/


//        Log.e("name", msg.getString("name"));
//        Log.e("riderID",msg.getString("ride_id"));
//        Log.e("mobile",msg.getString("mobile"));
//        Log.e("lat",msg.getString("lat"));
//        Log.e("lng", msg.getString("lng"));
//        Log.e("from_loc",msg.getString("from_loc"));
//        Log.e("to_loc",msg.getString("to_loc"));
//        Log.e("time",msg.getString("time"));

        notificationManager.notify(num, notificationBuilder.build());

        if(Utility.isAppRunning(this,getApplicationContext().getPackageName())){
            Intent intent1 = new Intent(this, RideNotificationActivity.class);
            intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            startActivity(intent1);
        }

        cancelNotification();



    }

    public void cancelNotification(){
        Handler handler = new Handler(Looper.getMainLooper());

        handler.postDelayed(new Runnable() {

          public  void run() {
                // Run your task here
              notificationManager.cancel(num);
            }
        }, 20000 );
    }

    private void showNotification(String message) {

        Log.e("MyMessage",message);
        Intent i = new Intent(this,RideNotificationActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,i, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setAutoCancel(true)
                .setContentTitle("FCM Test")
                .setContentText(message)
                .setSmallIcon(R.drawable.common_google_signin_btn_icon_dark)
                .setContentIntent(pendingIntent);

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        manager.notify(0,builder.build());
    }




}
